# BIANCA report

This is a set of scripts to generate .html reports for BIANCA (or other images)

**SETUP PYTHONPATH:**
Assuming you have the scripts in "bianca_report-master" folder, type:

    export PYTHONPATH=<my_absolute_path>/bianca_report-master/

Then you can use the scripts as follows:

## slicesview
For each pair of images in the list, displays *underlay* summary image first alone and then with *overlay* image (e.g BIANCA output) in a .html file.

**Example simple call:**
    
    fslpython -m bianca.slicesview my_output_dir -i img_pairs.txt

**Arguments:**

outdir (required): 1st argument is output directory name

-i, --infile (required): txt file containing pairs of images to display in format *underlay* *overlay*. *Overlay* can be non-binary (e.g. probability map)

-x, --orientation : Plane for image display ('Axial', 'Coronal', 'Sagittal') (default: automatically determines the plane with best resolution, if isotropic shows in Axial)

-n, --nslices : Number of slices to display (default=6)

-t, --interp : Perform interpolation before generating images. (default: displays at true resolution)

## report_single
Creates single subject report (summary images, volume measures) in .html and .json

**Example call:**
    
    fslpython -m bianca.report_single sub-001_report -s sub-001 -u sub-001_FLAIR.nii.gz \
    -i sub-001_biancathr09.nii.gz -p sub-001_biancaprobabmap.nii.gz \
    -b sub-001_brainmask.nii.gz -w sub-001_biancaWMmask.nii.gz \
    -c sub-001_ventmask_2_FLAIR_bin.nii.gz

**Arguments:**

outdir : output directory name

-s,--subname :subject name

-i,--infile : required, Lesion mask (thresholded BIANCA output)

-u,--underlay, required, Original image (underlay, e.g. FLAIR)

-p,--probab : Lesion probability map (unthresholded BIANCA output)(optional)

-b,--brainmask : Brain mask (to display and use to normalise lesion volumes) (optional)

-w,--wmask : White matter mask (output of make_bianca_mask)(optional))

-c,--ventmask : Ventricles mask (output of make_bianca_mask)(optional))

-x,--orientation : Plane for image display ('Axial', 'Coronal', 'Sagittal') (default: automatically determines the plane with best resolution, if isotropic shows in Axial)

-n, --nslices : Number of slices to display (default=6)

-t, --interp : Perform interpolation before generating images. (default: displays at true resolution)

## report_group
Given a list (.txt file) containing the paths to the single-subject report directories, it creates a group report by reading information from single subject reports (json)

**Example call:**
    
    fslpython -m bianca.report_group my_group_report -i input_list.txt

**Arguments:**

outdir : output directory name

-i, --infile (required): txt file containing paths to single subject report folders (where to find json files)

## report_training
Given a list with *underlay*, *automated lesion mask* and *manual mask* for each subject, creates a .html report displaying *underlay* alone and then with overlaping masks. At the bottom, overlap measures are calculated and summarised.

**Example list generation:**

    for sub in sub-001 sub-002 sub-003 ; do 
    echo ${sub}_FLAIR.nii.gz ${sub}_biancathr09.nii.gz \ 
    ${sub}_WMH_manualmask.nii.gz >> training_list.txt
    done

**Example call:**

    fslpython -m bianca.report_training my_training_report -i training_list.txt

**Arguments:**

outdir : output directory name

-i,--infile, required, txt file containing 3 columns of images: *underlay* *bianca mask* *ground_truth*

-x,--orientation : Plane for image display ('Axial', 'Coronal', 'Sagittal') (default: automatically determines the plane with best resolution, if isotropic shows in Axial)

-n, --nslices : Number of slices to display (default=6)

-t, --interp : Perform interpolation before generating images. (default: displays at true resolution)
