#!/usr/bin/env fslpython
#=========================================================================================
# FSL bianca report
# Ludovica Griffanti
# 23-02-2018, FMRIB, Oxford
##=========================================================================================
"""
Created on Tue Feb 20 09:22:09 2018
Bianca training report
@author: ludovica
"""

import sys
import argparse
import nibabel as nib
import numpy as np
import os
import os.path as op
import jinja2
import bianca.slicesutils as ut
#from nipype.algorithms import metrics as na

usage_string = """
Bianca training report: for each subject in the list, displays overlap images and calculates overlap measures, summarised at the bottom
"""

mod_dir = op.dirname(op.abspath(__file__))
    
def parse_args(args):
    parser = argparse.ArgumentParser(description=usage_string)
    parser.add_argument('outdir', help='output directory name')
    parser.add_argument('-i','--infile', required=True, help='txt file containing 3 columns of images: <underlay> <bianca mask> <ground_truth>')
    parser.add_argument('-x','--orientation', choices=['Axial', 'Coronal', 'Sagittal'],help='Plane for image display (default: automatically determines the plane with best resolution, if isotropic shows in Axial')
    parser.add_argument('-n', '--nslices', default=6, help='Number of slices to display (default=6)')
    parser.add_argument('-t', '--interp', action='store_true', help='Perform interpolation before generating images. (default: displays at true resolution)')
    return parser.parse_args(args)

def main(args=None):
    
    if args is None:
        args = sys.argv[1:]
    
    args = parse_args(args)
    outdir = args.outdir
    infile = args.infile
    orientation = args.orientation
    nslices = args.nslices
    interp = args.interp
    
    outdir=op.abspath(outdir)
    while op.isdir(outdir):
        outdir = outdir+'+'
    print('Output directory: '+outdir)    
    os.mkdir(outdir) 
    
    #read imputs and separate them
    with open(infile, 'r') as input_file:
        #divide in 3 columns
        threecol=[line.strip().split() for line in input_file.readlines()]
        col1, col2, col3 = zip(*threecol)
    
    nsub=len(threecol)
    listsub=np.arange(nsub)
    figname=[]
    new_list = []
    
    # For each subject create .png images
    for sub in listsub:   
        print(col1[sub])
        img = nib.load(col1[sub])
        mask = nib.load(col2[sub])
        gtruth = nib.load(col3[sub])
        
        maskdata=mask.get_data()
        gtdata=gtruth.get_data()
        
        # Prepare slices for all 3 images
        allSlices,pixdims = ut.prepareslices(img,orientation,nslices)
        allmaskSlices,pixdims2 = ut.prepareslices(mask,orientation,nslices)
        allgtSlices,pixdims3 = ut.prepareslices(gtruth,orientation,nslices)  
        
        # Figures
        #underlay alone      
        figname=outdir+'/biancaunderlay'+str(sub+1)+'.png'
        ut.createpng(allSlices,pixdims,figname,interp)
        
#        #underlay + mask         
#        figname2=outdir+'/bianca'+str(sub)+'.png'
#        ut.createpng_withoverlay(allSlices,allmaskSlices,pixdims2,figname2,interp)
#
#        # underlay + ground truth       
#        figname3=outdir+'/groundtruth'+str(sub)+'.png'
#        ut.createpng_withoverlay(allSlices,allgtSlices,pixdims3,figname3,interp)
        
        # underlay + mask + ground truth      
        figname4=outdir+'/overlap'+str(sub+1)+'.png'
        ut.createpng_twomasks(allSlices,allgtSlices,allmaskSlices,pixdims,figname4,interp)

        stats={}
        
        ndigits=6
        pixdim = np.array(mask.header.get_zooms())       
        stats['voxmm3'] = float(np.round(float(pixdim[0]) * float(pixdim[1]) * float(pixdim[2]),3))
        stats['totvox'] = int(np.count_nonzero(maskdata))
        stats['totvol'] = float(np.round(stats['totvox'] * stats['voxmm3'],ndigits))        
        stats['gt_totvox'] = int(np.count_nonzero(gtdata))
        stats['gt_totvol'] = float(np.round(stats['gt_totvox'] * stats['voxmm3'],ndigits))
        stats['diff_vol'] = float(np.round(stats['totvol'] - stats['gt_totvox'],ndigits))
        k=1        
        stats['dice'] = float(np.round(np.sum(maskdata[gtdata==k]==k)*2.0/(np.sum(maskdata[maskdata==k]==k)+ np.sum(gtdata[gtdata==k]==k)),ndigits))
        
        ###### TO DO: add more metrics
#        overlap = na.Overlap()
#        overlap.inputs.volume1 = col2[sub]
#        overlap.inputs.volume2 = col3[sub]
#        res = overlap.run()
#        print(res)
#        stats['dice']=res.dice
        
        #store info for each subject in for html
        item = (op.relpath(figname,outdir),)
        item += (op.relpath(figname4,outdir),)
        item += ('subject'+str(sub+1),)
        item += (stats,)
        new_list += [item]
    
    #calculates average stats 
    globstats = {}
    ndigits=6
    # average
    globstats['totvox'] = np.round(np.mean([x[3]['totvox'] for x in new_list]),ndigits)
    globstats['totvol'] = np.round(np.mean([x[3]['totvol'] for x in new_list]),ndigits)
    globstats['gt_totvox'] = np.round(np.mean([x[3]['gt_totvox'] for x in new_list]),ndigits)
    globstats['gt_totvol'] = np.round(np.mean([x[3]['gt_totvol'] for x in new_list]),ndigits)
    globstats['diff_vol'] = np.round(np.mean([x[3]['diff_vol'] for x in new_list]),ndigits)
    globstats['dice'] = np.round(np.mean([x[3]['dice'] for x in new_list]),ndigits)
    
    # Now generating html with all subjects
    with open(op.join(mod_dir, 'report_training_templ.html'), 'rt') as f:
        template = jinja2.Template(f.read())
    
    titlehtml='TRAINING SUBJECTS FILE: '+infile
    outname_html = op.join(outdir,'index.html')
    d = {'title': titlehtml, 'images':new_list, 'globstats':globstats}
    with open(outname_html, 'w') as f:
        f.write(template.render(d))

    
if __name__ == "__main__":
    main()
