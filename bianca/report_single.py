#!/usr/bin/env fslpython
#=========================================================================================
# FSL bianca report
# Ludovica Griffanti
# 23-02-2018, FMRIB, Oxford
##=========================================================================================
"""
Created on Tue Feb 20 09:22:09 2018
Bianca Single-subject Report
@author: ludovica
"""
import sys
import argparse
import nibabel as nib
import bianca.slicesutils as ut
import os
import os.path as op
import jinja2
import json

usage_string = """
Bianca Single-subject Report: creates single subject report (summary images, volume measures) in html and json

"""

mod_dir = op.dirname(op.abspath(__file__))

def parse_args(args):
    parser = argparse.ArgumentParser(description=usage_string)
    parser.add_argument('outdir', help='output directory name')
    parser.add_argument('-s','--subname', help='subject name')    
    parser.add_argument('-i','--infile', required=True, help='Lesion mask (thresholded BIANCA output)')
    parser.add_argument('-u','--underlay', required=True, help='Original image (underlay, e.g. FLAIR)')
    parser.add_argument('-p','--probab', default=None, help='Lesion probability map (unthresholded BIANCA output)(optional)')
    parser.add_argument('-b','--brainmask', default=None, help='Brain mask (to display and use to normalise lesion volumes) (optional)')
    parser.add_argument('-w','--wmask', default=None, help='White matter mask (output of make_bianca_mask)(optional)')
    parser.add_argument('-c','--ventmask', default=None, help='Ventricles mask (output of make_bianca_mask)(optional)')
    parser.add_argument('-x', '--orientation', choices=['Axial', 'Coronal', 'Sagittal'],help='Plane for image display (default: automatically determines the plane with best resolution, if isotropic shows in Axial')
    parser.add_argument('-n', '--nslices', default=6, help='Number of slices to display (default=6)')
    parser.add_argument('-t', '--interp', action='store_true', help='Perform interpolation before generating images. (default: displays at true resolution)')
    return parser.parse_args()

def main(args=None):
      
    if args is None:
        args = sys.argv[1:]
    
    args = parse_args(args)
    outdir = args.outdir
    subname = args.subname
    infile = args.infile
    underlay = args.underlay
    probab = args.probab
    brainmask = args.brainmask
    wmask = args.wmask
    ventmask = args.ventmask  
    orientation = args.orientation
    nslices = args.nslices
    interp = args.interp
    
    outdir=op.abspath(outdir)
    while op.isdir(outdir):
        outdir = outdir+'+'
    print('Output directory: '+outdir)    
    os.mkdir(outdir)     
    
    #loading compulsory data
    img = nib.load(underlay)
    maskimg = nib.load(infile)
    #prepare all slices for all images
    allSlices, pixdims = ut.prepareslices(img,orientation,nslices)
    allmaskSlices,pixdims2 = ut.prepareslices(maskimg,orientation,nslices)
    
    #load optional arguments and prepare slices
    #if probability map was passed as argument and the file exists
    if probab is not None:
        probimg = nib.load(probab)
        allprobSlices,pixdims3 = ut.prepareslices(probimg,orientation,nslices)
    if brainmask is not None:
        brainimg = nib.load(brainmask)
        allbrainSlices,pixdims4 = ut.prepareslices(brainimg,orientation,nslices)
    else:
        brainimg=None
    if wmask is not None:
        wmimg = nib.load(wmask)
        allWmSlices,pixdims5 = ut.prepareslices(wmimg,orientation,nslices)
    if ventmask is not None:
        ventimg = nib.load(ventmask)
        allVentSlices,pixdims6 = ut.prepareslices(ventimg,orientation,nslices)
    
    # storing info for html
    info={}
    info['infile'] = infile
    
    # create .png images and stores info for html
    #1)underlay
    figname=outdir+'/biancaunderlay.png'
    ut.createpng(allSlices,pixdims,figname,interp)
    info['biancaunderlay'] = op.relpath(figname,outdir)
    
    #2)overlay 
    figname2=outdir+'/bianca.png'
    ut.createpng_withoverlay(allSlices,allmaskSlices,pixdims2,figname2,interp)
    info['bianca'] = op.relpath(figname2,outdir)
    
    #3)probability map
    if probab is not None:
        figname3=outdir+'/biancaprob.png'
        ut.createpng_withoverlay(allSlices,allprobSlices,pixdims2,figname3,interp)
        info['biancaprob'] = op.relpath(figname3,outdir)
    #4)masks map = any combination of brain mask, WM mask and ventricles mask
    if brainmask is not None or wmask is not None or ventmask is not None:
        figname4=outdir+'/biancamasks.png'
        info['biancamasks']= op.relpath(figname4,outdir)
        
    if brainmask is not None:
        n_statdisplay=True
        # if also other images are present, display 3 masks
        if wmask is not None and ventmask is not None:
            ut.createpng_threemasks(allSlices,allbrainSlices,allWmSlices,allVentSlices,pixdims,figname4,interp)
        #if brain mask and WMmask (no vent mask)
        elif wmask is not None and ventmask is None:
            ut.createpng_twomasks(allSlices,allWmSlices,allbrainSlices,pixdims,figname4,interp)
        #if brain mask and ventmask (no WMmask)
        elif wmask is None and ventmask is not None:
            ut.createpng_twomasks(allSlices,allVentSlices,allbrainSlices,pixdims,figname4,interp)
        else:
            # otherwise only display brain mask
            ut.createpng_withoverlay(allSlices,allbrainSlices,pixdims,figname4,interp)           
    else:
        n_statdisplay=False
        # if any of the other two are still present creates image
        if wmask is not None and ventmask is not None:
            ut.createpng_twomasks(allSlices,allWmSlices,allVentSlices,pixdims,figname4,interp)
        # otherwise only displays one of them
        elif wmask is not None and ventmask is None:
            ut.createpng_withoverlay(allSlices,allWmSlices,pixdims,figname4,interp)
        elif wmask is  None and ventmask is not None:
            ut.createpng_withoverlay(allSlices,allVentSlices,pixdims,figname4,interp)
        
    #calculate stats from bianca output
    stats=ut.bianca_stats(maskimg,brainimg)
    info['stats'] = stats
    
    # Preparing info and saving them in json file
    data_json=ut.json_singlesub(subname,outdir,infile,stats,True)    
    json_outname=outdir + '/bianca_report.json'
    with open(json_outname, 'w') as fp:
        json.dump(data_json, fp, sort_keys=True, indent=4, separators=(',', ': '))

    # Now generating html
    with open(op.join(mod_dir, 'report_single_templ.html'), 'rt') as f:
        template = jinja2.Template(f.read())
        
    titlehtml='BIANCA report for subject : '+subname
    outname_html = op.join(outdir,'index.html')
    d = {'title': titlehtml, 'info':info,'n_statdisplay':n_statdisplay}
    with open(outname_html, 'w') as f:
        f.write(template.render(d))    
        
if __name__ == '__main__':
    main()
     