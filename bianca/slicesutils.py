#!/usr/bin/env fslpython
#=========================================================================================
# FSL bianca report
# Ludovica Griffanti
# 23-02-2018, FMRIB, Oxford
##=========================================================================================
"""
Created on Tue Feb 20 12:54:30 2018
Set of functions used in bianca report scripts
@author: ludovica
"""
import numpy as np
import matplotlib.pyplot as plt
from skimage import measure

def prepareslices(d,orientation=None,nslices=6):
    x = 0
    y = 1
    z = 2
    t = 3
    d0 = d.get_data()
    pixdim = np.array(d.header.get_zooms())
    #if no orientation is specified I select the best resolution
    if orientation is None:
        if pixdim[0]==pixdim[1]==pixdim[2]:
            orientation='Axial'
        elif pixdim.argmax()==0:
            orientation='Sagittal'
        elif pixdim.argmax()==1:
            orientation='Coronal'
        elif pixdim.argmax()==2:
            orientation='Axial'

    if len(d0.shape) == 3:
        d0 = d0[:, :, :, np.newaxis]
    # modify the 3D image so that it can always be sliced along the 3rd direction
    if orientation == "Sagittal":
        # generate indices of the slices to display
        #(4 more than requested as I will not display the first 2 and last 2 slices
        # as they are usually not very udeful)
        slicesindex=np.linspace(0, d0.shape[0]+1,nslices+4)
        d0 = np.transpose(d0, [z, y, x, t])
        pixdims=[pixdim[z],pixdim[y]]
    elif orientation == "Coronal":
        slicesindex=np.linspace(0, d0.shape[1]+1,nslices+4)
        d0 = np.transpose(d0, [z, x, y, t])
        pixdims=[pixdim[z],pixdim[x]]
    elif orientation == "Axial":
        slicesindex=np.linspace(0, d0.shape[2]+1,nslices+4)
        d0 = np.transpose(d0, [y, x, z, t])
        pixdims=[pixdim[y],pixdim[x]]
    else:
        raise ValueError('Unknown orientation value: ' + orientation)
    allSlices = np.flipud(d0[:, :,int(slicesindex[1]),0])    
    # append all slices horizontally (excluding first 2 and last 2)
    for i in slicesindex[3:-2]:
        mySlice = np.flipud(d0[:, :,int(i),0])
        allSlices = np.concatenate([allSlices, mySlice],axis=1)
    return allSlices, pixdims

def createpng(allSlices,pixdims,outputname,do_interpolation=True):
    if do_interpolation:
        imshowInterp  = 'bicubic'
        scalingFactor = 2
    else:
        imshowInterp  = 'nearest'
        scalingFactor = 1 
    fig = plt.figure(frameon=False)
    plt.axis('off')
    ax = fig.add_subplot(111)
    ax.imshow(allSlices,
              interpolation=imshowInterp,
              aspect=pixdims[0] / float(pixdims[1]),
              cmap='gray')
    ax.set_xticks([])
    ax.set_yticks([])
    ax.axis('off')
    pixels = (allSlices.shape[1] * pixdims[1] * scalingFactor,
              allSlices.shape[0] * pixdims[0] * scalingFactor)   
    fig.subplots_adjust(left=0, right=1.0, bottom=0, top=1)   
    fig.set_size_inches(pixels)
    fig.savefig(outputname, dpi=1)
    plt.close(fig) 
    return

def createpng_withoverlay(allSlices,allmaskSlices,pixdims,outputname,do_interpolation=True):
    if do_interpolation:
        imshowInterp  = 'bicubic'
        scalingFactor = 2
    else:
        imshowInterp  = 'nearest'
        scalingFactor = 1 
    fig = plt.figure(frameon=False)
    plt.axis('off')
    ax = fig.add_subplot(111)
    ax.imshow(allSlices,
              interpolation=imshowInterp,
              aspect=pixdims[0] / float(pixdims[1]),
              cmap='gray')
    masked = np.ma.masked_where(allmaskSlices == 0, allmaskSlices)
    ax.imshow(masked,
              interpolation=imshowInterp,
              aspect=pixdims[0] / float(pixdims[1]),
              cmap='autumn',alpha=.5)    
    ax.set_xticks([])
    ax.set_yticks([])
    ax.axis('off')
    #dpi = 1
    pixels = (allSlices.shape[1] * pixdims[1] * scalingFactor,
              allSlices.shape[0] * pixdims[0] * scalingFactor)   
    fig.subplots_adjust(left=0, right=1.0, bottom=0, top=1)   
    fig.set_size_inches(pixels)
    fig.savefig(outputname, dpi=1)
    plt.close(fig) 
    return
 
def createpng_threemasks(allSlices,allbrainSlices,allWmSlices,allVentSlices,pixdims,outputname,do_interpolation=True):
    if do_interpolation:
        imshowInterp  = 'bicubic'
        scalingFactor = 2
    else:
        imshowInterp  = 'nearest'
        scalingFactor = 1 
    fig = plt.figure(frameon=False)
    plt.axis('off')
    ax = fig.add_subplot(111)
    ax.imshow(allSlices,
              interpolation=imshowInterp,
              aspect=pixdims[0] / float(pixdims[1]),
              cmap='gray')    
    # brain mask in yellow
    maskedbm = np.ma.masked_where(allbrainSlices == 0, allbrainSlices)
    ax.imshow(maskedbm,
              interpolation=imshowInterp,
              aspect=pixdims[0] / float(pixdims[1]),
              cmap='Wistia',alpha=.5)  
    # WM mask in blue
    maskedwm = np.ma.masked_where(allWmSlices == 0, allWmSlices)
    ax.imshow(maskedwm,
              interpolation=imshowInterp,
              aspect=pixdims[0] / float(pixdims[1]),
              cmap='winter',alpha=.5) 
    # ventricles mask in red
    maskedvent = np.ma.masked_where(allVentSlices == 0, allVentSlices)
    ax.imshow(maskedvent,
              interpolation=imshowInterp,
              aspect=pixdims[0] / float(pixdims[1]),
              cmap='autumn',alpha=.5)   
    ax.set_xticks([])
    ax.set_yticks([])
    ax.axis('off')
    pixels = (allSlices.shape[1] * pixdims[1] * scalingFactor,
              allSlices.shape[0] * pixdims[0] * scalingFactor)   
    fig.subplots_adjust(left=0, right=1.0, bottom=0, top=1)   
    fig.set_size_inches(pixels)
    fig.savefig(outputname, dpi=1)
    plt.close(fig) 
    return

def createpng_twomasks(allSlices,allblueSlices,allredSlices,pixdims,outputname,do_interpolation=True):
    if do_interpolation:
        imshowInterp  = 'bicubic'
        scalingFactor = 2
    else:
        imshowInterp  = 'nearest'
        scalingFactor = 1 
    fig = plt.figure(frameon=False)
    plt.axis('off')
    ax = fig.add_subplot(111)
    ax.imshow(allSlices,
              interpolation=imshowInterp,
              aspect=pixdims[0] / float(pixdims[1]),
              cmap='gray')    
    # first mask in blue
    maskedwm = np.ma.masked_where(allblueSlices == 0, allblueSlices)
    ax.imshow(maskedwm,
              interpolation=imshowInterp,
              aspect=pixdims[0] / float(pixdims[1]),
              cmap='winter',alpha=.5) 
    # second mask in red
    maskedvent = np.ma.masked_where(allredSlices == 0, allredSlices)
    ax.imshow(maskedvent,
              interpolation=imshowInterp,
              aspect=pixdims[0] / float(pixdims[1]),
              cmap='autumn',alpha=.4)   
    ax.set_xticks([])
    ax.set_yticks([])
    ax.axis('off')
    pixels = (allSlices.shape[1] * pixdims[1] * scalingFactor,
              allSlices.shape[0] * pixdims[0] * scalingFactor)   
    fig.subplots_adjust(left=0, right=1.0, bottom=0, top=1)   
    fig.set_size_inches(pixels)
    fig.savefig(outputname, dpi=1)
    plt.close(fig) 
    return

def bianca_stats (mask,brainmask=None):
    ndigits=5
    maskdata=mask.get_data()
    stats = {}
    pixdim = np.array(mask.header.get_zooms())
    stats['voxmm3'] = float(np.round(float(pixdim[0]) * float(pixdim[1]) * float(pixdim[2]),3))
    stats['totvox'] = int(np.count_nonzero(maskdata))      
    stats['totvol'] = float(np.round(stats['totvox'] * stats['voxmm3'],ndigits))
        
    #get labeled image and max number of labels (i.e. number of clusters)
    labelmask,stats['nclusters']=measure.label(maskdata,return_num=True,connectivity=None)
    clustersprops=measure.regionprops(labelmask)
    areas = [p.area for p in clustersprops]
    avgsizevox=float(np.mean(areas))
    stats['avg_clustervol'] = float(np.round(avgsizevox * stats['voxmm3'],ndigits))
    stdsizevox=float(np.std(areas))
    stats['std_clustervol'] = float(np.round(stdsizevox * stats['voxmm3'],ndigits))
    minsizevox=float(np.min(areas))
    stats['min_clustervol'] = float(np.round(minsizevox * stats['voxmm3'],ndigits))
    maxsizevox=float(np.max(areas))
    stats['max_clustervol'] = float(np.round(maxsizevox * stats['voxmm3'],ndigits))
    #print(stats)
    if brainmask is not None:
        bmdata=brainmask.get_data()
        stats['bmvox'] = float(np.round(np.count_nonzero(bmdata),ndigits))
        stats['bmvol'] = float(np.round(stats['bmvox'] * stats['voxmm3'],ndigits))
        stats['n_totvol'] = float(np.round(stats['totvol'] / stats['bmvol'] * 100,ndigits))
        stats['n_avg_clustervol'] = float(np.round(stats['avg_clustervol'] / float(stats['bmvol']) * 100,ndigits))
        stats['n_std_clustervol'] = float(np.round(stats['std_clustervol'] / float(stats['bmvol']) * 100,ndigits))
        stats['n_min_clustervol'] = float(np.round(stats['min_clustervol'] / float(stats['bmvol']) * 100,ndigits))
        stats['n_max_clustervol'] = float(np.round(stats['max_clustervol'] / float(stats['bmvol']) * 100,ndigits))
    else:
        stats['bmvox'] = None
        stats['bmvol'] = None
        stats['n_totvol'] = None
        stats['n_avg_clustervol'] = None
        stats['n_std_clustervol'] = None
        stats['n_min_clustervol'] = None
        stats['n_max_clustervol'] = None
    return stats

def json_singlesub(subjectname,output_dir,mpath,stats,brainmask=None):  
    if brainmask is not None:
        data_json = {
            'report_dir':output_dir,
            'subject_name': subjectname,
            'biancamask_file': mpath,
#            'figname':'biancaunderlay.png',
#            'figname2':'bianca.png',
#            'figname3':'biancaprob.png',
#            'figname4':'biancamasks.png',
            'voxmm3': stats['voxmm3'],
            'totvox':stats['totvox'],
            'totvol':stats['totvol'],
            'nclusters':stats['nclusters'],
            'avg_clustervol':stats['avg_clustervol'],
            'std_clustervol':stats['std_clustervol'],
            'min_clustervol':stats['min_clustervol'],
            'max_clustervol':stats['max_clustervol'],
            'bmvol':stats['bmvol'],
            'n_totvol':stats['n_totvol'],
            'n_avg_clustervol':stats['n_avg_clustervol'],
            'n_std_clustervol':stats['n_std_clustervol'],
            'n_min_clustervol':stats['n_min_clustervol'],
            'n_max_clustervol':stats['n_max_clustervol'],       
        }
#    else:
#        data_json = {
#            'report_dir':output_dir,
#            'subject_name':subjectname,
#            'biancamask_file':mpath,
#            'figname':'biancaunderlay'+str(subjectname)+'.png',
#            'figname2':'bianca'+str(subjectname)+'.png',
#            'figname3':'biancaprob'+str(subjectname)+'.png',
#            'voxmm3':stats['voxmm3'],
#            'totvox':stats['totvox'],
#            'totvol':stats['totvol'],
#            'nclusters':stats['nclusters'],
#            'avg_clustervol':stats['avg_clustervol'],
#            'std_clustervol':stats['std_clustervol'],
#            'min_clustervol':stats['min_clustervol'],
#            'max_clustervol':stats['max_clustervol'],      
#        }
    return data_json