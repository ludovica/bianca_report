#!/usr/bin/env fslpython
#=========================================================================================
# FSL bianca report
# Ludovica Griffanti
# 23-02-2018, FMRIB, Oxford
##=========================================================================================
"""
Created on Tue Feb 20 09:22:09 2018
Bianca Group Report
@author: ludovica
"""
import sys
import argparse
import numpy as np
import os
import os.path as op
import jinja2
import json

usage_string = """
Bianca Group Report: creates a group report by reading information from single subject reports
"""

mod_dir = op.dirname(op.abspath(__file__))

def parse_args(args):
    parser = argparse.ArgumentParser(description=usage_string)
    parser.add_argument('outdir', help='output directory name')
    parser.add_argument('-i','--infile', required=True, help='txt file containing paths to single subject report folders (where to find json file)')
    return parser.parse_args()

def main(args=None):
    
    if args is None:
        args = sys.argv[1:]
        
    args = parse_args(args)
    
    outdir = args.outdir
    infile = args.infile
    # will calculate normalised stats only if all subjects have them
    n_statdisplay=True
    
    outdir=op.abspath(outdir)
    while op.isdir(outdir):
        outdir = outdir+'+'
    print('Output directory: '+outdir)    
    os.mkdir(outdir)    
    
    #read imput report folders        
    with open(infile, 'r') as input_file:
        jsonfiles=[line.strip()+'/bianca_report.json' for line in input_file.readlines()]
    
    nsub=len(jsonfiles)
    listsub=np.arange(nsub)
    new_list=[]
    #For each subject create .png images
    for sub in listsub:   
        # Read JSON file
        with open(jsonfiles[sub]) as data_file:
            data_loaded = json.load(data_file)
        stats={}
        stats["totvol"]=data_loaded["totvol"]
        stats["totvox"]=data_loaded["totvox"]
        stats["voxmm3"]=data_loaded["voxmm3"]
        stats["nclusters"]=data_loaded["nclusters"]
        stats["bmvol"]=data_loaded["bmvol"]
        stats["avg_clustervol"]=data_loaded["avg_clustervol"]
        stats["std_clustervol"]=data_loaded["std_clustervol"]
        stats["max_clustervol"]=data_loaded["max_clustervol"]
        stats["min_clustervol"]=data_loaded["min_clustervol"]
        if stats["bmvol"] is not None:
            stats["n_totvol"]=data_loaded["n_totvol"]    
            stats["n_avg_clustervol"]=data_loaded["n_avg_clustervol"]
            stats["n_std_clustervol"]=data_loaded["n_std_clustervol"]
            stats["n_max_clustervol"]=data_loaded["n_max_clustervol"]
            stats["n_min_clustervol"]=data_loaded["n_min_clustervol"]
        else:
            n_statdisplay=False
        #store info for each subject in for html
        #images with absolute path
        item = (op.join(data_loaded["report_dir"],'biancaunderlay.png'),)
        item += (op.join(data_loaded["report_dir"],'bianca.png'),)
        #subject title
        item += (data_loaded["subject_name"],)   
        #subject stats
        item += (stats,)
        new_list += [item]
        
    #calculates average stats 
    globstats = {}
    ndigits=6
    # average
    globstats['totvox'] = np.round(np.mean([x[3]['totvox'] for x in new_list]),ndigits)
    globstats['totvol'] = np.round(np.mean([x[3]['totvol'] for x in new_list]),ndigits)
    globstats['nclusters'] = np.round(np.mean([x[3]['nclusters'] for x in new_list]))
    globstats['avg_clustervol'] = np.round(np.mean([x[3]['avg_clustervol'] for x in new_list]),ndigits)
    globstats['std_clustervol'] = np.round(np.mean([x[3]['std_clustervol'] for x in new_list]),ndigits)
    globstats['min_clustervol'] = np.round(np.mean([x[3]['min_clustervol'] for x in new_list]),ndigits)
    globstats['max_clustervol'] = np.round(np.mean([x[3]['max_clustervol'] for x in new_list]),ndigits)
    if n_statdisplay==True:
        globstats['n_totvol'] = np.round(np.mean([x[3]['totvol'] for x in new_list]),ndigits)
        globstats['n_avg_clustervol'] = np.round(np.mean([x[3]['n_avg_clustervol'] for x in new_list]),ndigits)
        globstats['n_std_clustervol'] = np.round(np.mean([x[3]['n_std_clustervol'] for x in new_list]),ndigits)
        globstats['n_min_clustervol'] = np.round(np.mean([x[3]['n_min_clustervol'] for x in new_list]),ndigits)
        globstats['n_max_clustervol'] = np.round(np.mean([x[3]['n_max_clustervol'] for x in new_list]),ndigits)

    # Now generating html with all subjects
    with open(op.join(mod_dir, 'report_group_templ.html'), 'rt') as f:
        template = jinja2.Template(f.read())
    
    titlehtml='BIANCA group report for subjects in: '+infile
    outname_html = op.join(outdir,'index.html')
    d = {'title': titlehtml, 'images':new_list, 'globstats':globstats,'n_statdisplay':n_statdisplay}
    with open(outname_html, 'w') as f:
        f.write(template.render(d))
        
if __name__ == '__main__':
    main()