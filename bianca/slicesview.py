#!/usr/bin/env fslpython
#=========================================================================================
# FSL bianca_slicesview
# Ludovica Griffanti
# 22-02-2018, FMRIB, Oxford
##=========================================================================================
"""
Created on Tue Feb 20 09:22:09 2018

@author: ludovica
"""

import sys
import argparse
import nibabel as nib
import numpy as np
import os
import os.path as op
import jinja2

import bianca.slicesutils as ut

usage_string = """
Bianca slicesview: for each pair of images in the list, displays <underlay> summary image first alone and then with <overlay> image (e.g BIANCA output).
"""

mod_dir = op.dirname(op.abspath(__file__))

    
def parse_args(args):
    parser = argparse.ArgumentParser(description=usage_string)
    parser.add_argument('outdir', help='output directory name')
    parser.add_argument('-i','--infile', required=True, help='txt file containing pairs of images to display in format <underlay> <overlay>. <overlay> can be non-binary (e.g. probability map)')
    parser.add_argument('-x','--orientation', choices=['Axial', 'Coronal', 'Sagittal'],help='Plane for image display (default: automatically determines the plane with best resolution, if isotropic shows in Axial')
    parser.add_argument('-n', '--nslices', default=6, help='Number of slices to display (default=6)')
    parser.add_argument('-t', '--interp', action='store_true', help='Perform interpolation before generating images. (default: displays at true resolution)')
    return parser.parse_args(args)

def main(args=None):
    
    if args is None:
        args = sys.argv[1:]
    
    args = parse_args(args)
    outdir = args.outdir
    infile = args.infile
    orientation = args.orientation
    nslices = args.nslices
    interp = args.interp
    
    outdir=op.abspath(outdir)
    while op.isdir(outdir):
        outdir = outdir+'+'
    print('Output directory: '+outdir)    
    os.mkdir(outdir) 
    
    #read imput pairs and separate them
    with open(infile, 'r') as input_file:
        #divide in two columns
        twocol=[line.strip().split() for line in input_file.readlines()]
        col1, col2 = zip(*twocol)
    
    nsub=len(twocol)
    listsub=np.arange(nsub)
    figname=[]
    new_list = []
    
    # For each subject create .png images
    for sub in listsub:   
    #for sub in listsub[:4]:
        print(col1[sub])
        img = nib.load(col1[sub])
        mask = nib.load(col2[sub])
        
        #underlay
        allSlices,pixdims = ut.prepareslices(img,orientation,nslices)
        figname=outdir+'/biancaunderlay'+str(sub)+'.png'
        ut.createpng(allSlices,pixdims,figname,interp)
        
        #overlay
        allmaskSlices,pixdims2 = ut.prepareslices(mask,orientation,nslices)  
        figname2=outdir+'/bianca'+str(sub)+'.png'
        ut.createpng_withoverlay(allSlices,allmaskSlices,pixdims2,figname2,interp)
        
        #store info for each subject in for html
        #images
        item = (op.relpath(figname,outdir),)
        item += (op.relpath(figname2,outdir),)
        #subject title
        item += (col2[sub],)   
        new_list += [item]
        
    # Now generating html with all subjects
    with open(op.join(mod_dir, 'slicesview_templ.html'), 'rt') as f:
        template = jinja2.Template(f.read())
    
    titlehtml='INPUT FILE: '+infile
    outname_html = op.join(outdir,'index.html')
    d = {'title': titlehtml, 'images':new_list}
    with open(outname_html, 'w') as f:
        f.write(template.render(d))

    
if __name__ == "__main__":
    main()
